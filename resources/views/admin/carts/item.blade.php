@extends( 'layouts.admin' )

@section('title')
Cart
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <table class="table is-striped">
                <thead>
                    <tr>
                        <th>Item Name</th>
                        <th>Item Qty</th>
                        <th>Item Price</th>
                    </tr>
                </thead>
                <tbody>
                    @if(Cart::count()  > 0)
                        @foreach(Cart::content() as $item)
                       <tr>
                        <td>{{$item->name}}</td>
                        <td>{{$item->qty}}</td>
                        <td>{{$item->price}}</td>
                    </tr>
                        @endforeach
                    
                    @endif
                <tr>
                    <td></td>
                    <td><strong>SUBTOTAL</strong></td>
                    <td>{{ Cart::subtotal() }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td><strong>TAX</strong></td>
                    <td>{{ Cart::tax() }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td><strong>TOTAL</strong></td>
                    <td>{{ Cart::total() }}</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
@endsection