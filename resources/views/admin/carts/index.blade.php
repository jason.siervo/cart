@extends( 'layouts.admin' )

@section('title')
Order Management
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
                        <h6>Order MANAGEMENT</h6>                        
                    <br>


                <table class="table is-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($orders->count() > 0)
                        @foreach( $orders as $order )
                        <tr>
                            <td>{{ $order->identifier }}</td>
                            <td>{{ $order->created_at->diffForHumans() }}</td>

                            $work->due_date->diffForhumans()
                            <td>
                            <a href="/admin/order/{{ $order->identifier }}" class="btn btn-info">View</a>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
                {{$orders->links()}}
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
@endsection