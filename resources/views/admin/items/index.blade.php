@extends( 'layouts.admin' )

@section('title')
Item Management
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
                        <h6>ITEM MANAGEMENT</h6>                        
                    <br>
                    <a href="/admin/items/create" class="btn btn-success">Create Item</a>


                <table class="table is-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Item Name</th>
                            <th>Item Image</th>
                            <th>Item Price</th>
                            <th>Item QTY</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($items->count() > 0)
                        @foreach( $items as $item )
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->item_name }}</td>
                            <td><img src="/images/small/{{basename(asset($item->item_image))}}" alt=""></td>
                            <td>{{ $item->item_price }}</td>
                            <td>{{ $item->item_qty }}</td>
                            <td>
                            <a href="/admin/item/{{ $item->id }}" class="btn btn-info">View</a>
                            <a href="/admin/item/{{ $item->id }}/edit" class="btn btn-warning">Edit</a>
                            <button class="btn btn-danger" onclick="event.preventDefault(); document.getElementById('delete-{{$item->id}}').submit();">Delete</button>
                            <form action="/admin/item/{{ $item->id }}/delete" method="post" style="display-hidden" id="delete-{{$item->id}}">
                                {{csrf_field()}}
                            </form>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
                {{$items->links()}}
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
@endsection