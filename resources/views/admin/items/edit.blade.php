@extends( 'layouts.admin' )

@section('title')
Edit Item
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <form action="/admin/item/{{$item->id}}/update" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="">Item Name <span class="text-danger"><strong>*</strong></span></label>
                    <input type="text" name="item_name" id="" class="form-control" value="{{$item->item_name}}">
                    <small class="help text-danger"></small>
                </div>
                <div class="form-group">
                <label for="">Item Description <span class="text-danger"><strong>*</strong></span></label>
                    <textarea name="item_description" id="" cols="30" rows="10" class="form-control">{{$item->item_description}}</textarea>
                    <small class="help text-danger"></small>
                </div>
                <div class="form-group">
                <label for="">Item Image <span class="text-danger"><strong>*</strong></span></label>
                    <input type="file" name="item_image" id="" class="form-control">
                    <small class="help text-danger"></small>
                </div>
                <div class="form-group">
                <label for="">Item Price <span class="text-danger"><strong>*</strong></span></label>
                    <input type="text" name="item_price" id="" class="form-control" value="{{$item->item_price}}">
                    <small class="help text-danger"></small>
                </div>
                <div class="form-group">
                <label for="">Item Qty <span class="text-danger"><strong>*</strong></span></label>
                    <input type="text" name="item_qty" id="" class="form-control" value="{{$item->item_qty}}">
                    <small class="help text-danger"></small>
                </div>
                <div class="form-group"><button class="btn btn-success">Submit</button></div>
                {{ csrf_field()  }}
            </form>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
@endsection