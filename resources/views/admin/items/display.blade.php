@extends( 'layouts.admin' )

@section('title')
{{$item->name}}
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="card">
                <img src="{{asset($item->item_image)}}" alt="" class="card-img top">
                <div class="card-body">
                    <h3 class="card-title">{{$item->item_name}}</h3>
                    <h6 class="card-subtitle">PHP {{$item->item_price}} | Qty {{$item->item_qty}}</h6>
                    <p class="card-text">{{$item->item_description}}</p>
                   
                </div>
                <div class="card-footer">
                        {{$item->created_at->diffForHumans()}}
                    </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
@endsection