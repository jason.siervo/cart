@extends( 'layouts.admin' )

@section('title')
{{$user->name}}
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">{{$user->name}}</h3>
                    <h6 class="card-subtitle">{{$user->email}} |  {{$user->isAdmin? 'Administrator' : 'User'}}</h6>
                </div>
                <div class="card-footer">
                        {{$user->created_at->diffForHumans()}}
                    </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
@endsection