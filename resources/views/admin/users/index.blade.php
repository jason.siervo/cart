@extends( 'layouts.admin' )

@section('title')
User Management
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <h6>USER MANAGEMENT</h6>
            <table class="table is-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Full Name</th>
                        <th>Email</th>
                        <th>Created On</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if($users->count() > 0)
                    @foreach( $users as $user )
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->created_at->diffForHumans() }}</td>
                        @if(!$user->isAdmin)
                        <td><a href="/admin/user/{{ $user->id }}/admin" class="btn btn-warning">Make Admin</a></td>
                        @endif
                        <td><a href="/admin/user/{{ $user->id }}" class="btn btn-info">View</a></td>                        
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
                    {{$users->links()}}
        </div>
        <div class="col-md-2"></div> 
    </div>
</div>
@endsection