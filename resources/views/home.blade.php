@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if($items->count() > 0)
                @foreach($items as $item)
                <div class="card">
                <img src="/{{$item->item_image}}" alt="" class="card-img top">
                <div class="card-body">
                    <h3 class="card-title">{{$item->item_name}}</h3>
                    <h6 class="card-subtitle">PHP {{$item->item_price}} | Qty {{$item->item_qty}}</h6>
                    <p class="card-text">{{$item->item_description}}</p>
                    <a href="/cart/add/{{$item->id}}" class="btn btn-success"><i class="fas fa-cart-plus"></i></a>   
                </div>
                <div class="card-footer">
                        {{$item->created_at->diffForHumans()}}
                    </div>
            </div>
            <br>
                @endforeach
            @else
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">No Items found</h1>
                    </div>
                </div>
            @endif
        </div>
        {{ $items->links() }}
    </div>
</div>
@endsection
