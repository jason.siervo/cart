@extends( 'layouts.app' )

@section('title')
Cart
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <table class="table is-striped">
                <thead>
                    <tr>
                        <th>Item Name</th>
                        <th>Item Qty</th>
                        <th>Item Price</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @if(Cart::count() > 0)
                        @foreach(Cart::content() as $item)
                       <tr>
                        <td>{{$item->name}}</td>
                        <td>{{$item->qty}}</td>
                        <td>{{$item->price}}</td>
                        <td>
                            <a href="/cart/delete/{{$item->rowId}}" class="btn btn-danger">X</a>
                            <a href="/cart/edit/{{$item->rowId}}" class="btn btn-warning">Edit</a>
                        </td>
                    </tr>
                        @endforeach
                    
                    @endif
                <tr>
                    <td></td>
                    <td><strong>SUBTOTAL</strong></td>
                    <td>{{ Cart::subtotal() }}</td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td><strong>TAX</strong></td>
                    <td>{{ Cart::tax() }}</td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td><strong>TOTAL</strong></td>
                    <td>{{ Cart::total() }}</td>
                    <td></td>
                </tr>
                </tbody>
            </table>

        <center>
            <form action="/cart/store" method="post">
                {{csrf_field()}}
            <a href="/cart/clear" class="btn btn-danger">Clear afaCart</a>
            </form>

            <form method="post" action="{!! URL::to('paypal') !!}">
                {{csrf_field()}}
                <input type = "hidden" name = "subtotal" value = "{{ Cart::subtotal() }}">
                <input type = "hidden" name = "tax" value = "{{ Cart::tax() }}">
                <input type = "hidden" name = "total" value = "{{ Cart::total() }}">
                <button class="btn btn-success" input type="submit" value="submit">Place Order</button>
            </form>
        </center>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
@endsection