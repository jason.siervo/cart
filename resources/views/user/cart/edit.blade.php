@extends( 'layouts.app' )

@section('title')
Create Item
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
                    @foreach($items as $item)
                <div class="form-group">
                    <label for="">Item Name <span class="text-danger"><strong>*</strong></span></label>
                    {{$item->name}}
                    <small class="help text-danger"></small>
                </div>
                <div class="form-group">
                <label for="">Item Price <span class="text-danger"><strong>*</strong></span></label>
                    {{$item->price}}
                    <small class="help text-danger"></small>
                </div>
            <form action="/cart/update/{{$item->rowId}}" method="post" enctype="multipart/form-data">
                <div class="form-group">
                <label for="">Item Qty <span class="text-danger"><strong>*</strong></span></label>
                    <input type="text" name="qty" id="" class="form-control">
                    <small class="help text-danger"></small>
                </div>
                <div class="form-group"><button class="btn btn-success">Submit</button></div>
                {{ csrf_field()  }}
            </form>
        </div>
                    @endforeach
        <div class="col-md-2"></div>
    </div>
</div>
@endsection