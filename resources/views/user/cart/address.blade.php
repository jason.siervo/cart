@extends( 'layouts.app' )

@section('title')
Cart
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
        
            @foreach ($inputs as $input)

                {{$input->subtotal}}
                {{$input->tax}}
                {{$input->total}}
                
            @endforeach
        
        </div>
        <div class="col-md-2"></div>
    <div>
</div>
@endsection