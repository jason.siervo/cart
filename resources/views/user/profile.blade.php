@extends( 'layouts.app' )

@section('title')
{{Auth::user()->name}}
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">{{Auth::user()->name}}</h3>
                    <h6 class="card-subtitle">{{Auth::user()->email}} |  {{Auth::user()->isAdmin? 'Administrator' : 'User'}}</h6>
                </div>
                <div class="card-footer">
                        {{Auth::user()->created_at->diffForHumans()}}
                    </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
@endsection