<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Item;
use Auth;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['items'] = Item::paginate( 10 );
        
        return view( 'admin.items.index', $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'admin.items.create' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {

        $request->validate([
            'item_name' => 'required',
            'item_description' => 'required',
            'item_image' => 'required | file',
            'item_price' => 'required | numeric',
        ]);

        if( $request->hasFile( 'item_image' ) ){
            $filename = md5(trim($request->item_image->getClientOriginalName() ));
            $request->file( 'item_image' )->move( 'img/items', $filename.'.'.$request->file('item_image')->getClientOriginalExtension() );

            Item::create([
                'item_name' => $request->item_name,
                'item_description' => $request->item_description,
                'item_image' => 'img/items/' . $filename . '.' .$request->file('item_image')->getClientOriginalExtension(),
                'item_price' => $request->item_price,
                'item_qty' => $request->item_qty,             
            ]);
            
            return redirect('admin/items');
            
        } 
        else {

            return back()->withInput();

        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( $id )
    {
        $data['item'] = Item::find( $id );

        return view( 'admin.items.display', $data );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['item'] = Item::find( $id );

        return view( 'admin.items.edit', $data );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {
        
        $request->validate([
            'item_price' => 'numeric',
            'item_qty' => 'numeric',
        ]);

        if( $request->hasFile( 'item_image' ) ){
            $filename = md5(trim($request->item_image->getClientOriginalName() ));
            $request->file( 'item_image' )->move( 'img/items', $filename.'.'.$request->file('item_image')->getClientOriginalExtension() );
            
            Item::where( 'id',$id )->update([
                'item_name' => $request->item_name,
                'item_description' => $request->item_description,
                'item_image' => 'img/items/' . $filename . '.' .$request->file('item_image')->getClientOriginalExtension(),
                'item_price' => $request->item_price,
                'item_qty' => $request->item_qty,
            ]);
            
            return redirect('admin/items');
            
        } 
        else {

            Item::where( 'id',$id )->update([
                'item_name' => $request->item_name,
                'item_description' => $request->item_description,
                'item_price' => $request->item_price,
                'item_qty' => $request->item_qty,
            ]);
            
            return redirect('admin/items');

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        Item::where( 'id',$id )->delete();

        return redirect('admin/items');
    }
}
