<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cart;
use App\ShoppingCart;
class CartController extends Controller
{
    public function orders(){
        $data['orders'] = ShoppingCart::latest()->paginate(5);
        return view('admin.carts.index',$data);
    }

    public function cart($id){
        Cart::restore($id);
        return view('admin.carts.item');
    }
}
