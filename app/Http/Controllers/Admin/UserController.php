<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;


class UserController extends Controller
{
    public function index()
    {
        $data['users'] = User::latest()->paginate( 10 );

        return view( 'admin.users.index', $data );
    }

    public function display($id)
    {
        $data['user'] = User::find( $id );

        return view( 'admin.users.display', $data );
    }

    public function admin($id)
    {
        User::where('id',$id)->update(['isAdmin' => TRUE]);
        return redirect()->back();
    }
}
