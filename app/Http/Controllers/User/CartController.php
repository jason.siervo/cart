<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Item;
use Cart;
use Auth;

class CartController extends Controller
{
    public function add($id)
    {
        $item = Item::find($id);
        Cart::add( $item->id, $item->item_name,1,$item->item_price );

        return back();
    }

    public function display()
    {
        $data['cart'] = Cart::content();    
        return view('user.cart.display',$data);
    }

    public function clear()
    {
        Cart::destroy();    
        return back();
    }

    public function delete($id)
    {
        Cart::remove($id);
        return back();   
    }

    public function edit($id){
        $data['items'] = Cart::search(function($cartItem,$id){
            return $cartItem->rowId == $id;
        });
        
        return view('user.cart.edit',$data);
    }

    public function update($id, Request $request){
        $request->validate([
            'qty' => 'required|numeric'
        ]);
        
        Cart::update($id,$request->qty);
        return redirect('cart');
    }

    public function store()
    {
        //dd('hello');
        Cart::store(Auth::user()->email);
        Cart::destroy();
        return back();
    }
}
