<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class DashboardController extends Controller
{
    public function index() {
        if(Auth::check() && Auth::user()->isAdmin)
        {
            return view('admin.index');
        } else {
            return redirect('home');
        }
    }
}
