<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Item;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->isAdmin)
        {
            $data['items'] = Item::latest()->where('item_price','!=','0')->paginate(5);
            return view('home', $data);
        } else {
            return redirect('/admin/dashboard');
        }
    }
}
