-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 06, 2018 at 03:20 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cart_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_price` double(8,2) NOT NULL,
  `item_qty` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `item_name`, `item_description`, `item_image`, `item_price`, `item_qty`, `created_at`, `updated_at`) VALUES
(9, 'Hasselback S\'mores Cake', 'Sweet', 'img/items/156005c5baf40ff51a327f1c34f2975b.jpg', 55.00, 13, '2018-06-19 10:22:06', '2018-06-19 10:22:06'),
(10, 'Chocolate Peppermint Brownie', 'Minty', 'img/items/799bad5a3b514f096e69bbc4a7896cd9.jpg', 89.00, 10, '2018-06-19 10:22:39', '2018-06-19 10:22:39'),
(11, 'Pink Ice Cream', 'Pinkyy', 'img/items/d0096ec6c83575373e3a21d129ff8fef.jpg', 99.00, 12, '2018-06-19 10:23:24', '2018-06-19 10:23:24'),
(12, 'Tiramisu Roll Cake', 'roll', 'img/items/032b2cc936860b03048302d991c3498f.jpg', 110.00, 12, '2018-06-19 10:23:53', '2018-06-19 10:23:53'),
(13, 'aaaasa', 'dadda', 'img/items/156005c5baf40ff51a327f1c34f2975b.jpg', 2223.00, 22, '2018-06-25 18:10:00', '2018-06-25 18:10:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2018_03_13_042140_create_items_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcart`
--

CREATE TABLE `shoppingcart` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instance` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shoppingcart`
--

INSERT INTO `shoppingcart` (`id`, `identifier`, `instance`, `content`, `created_at`, `updated_at`) VALUES
(4, 'jordyn.renner@example.com', 'default', 'O:29:\"Illuminate\\Support\\Collection\":1:{s:8:\"\0*\0items\";a:1:{s:32:\"c42f6beec9c93fd6afea6eb0684aa99a\";O:32:\"Gloudemans\\Shoppingcart\\CartItem\":8:{s:5:\"rowId\";s:32:\"c42f6beec9c93fd6afea6eb0684aa99a\";s:2:\"id\";i:9;s:3:\"qty\";i:1;s:4:\"name\";s:12:\"Krabby Patty\";s:5:\"price\";d:100;s:7:\"options\";O:39:\"Gloudemans\\Shoppingcart\\CartItemOptions\":1:{s:8:\"\0*\0items\";a:0:{}}s:49:\"\0Gloudemans\\Shoppingcart\\CartItem\0associatedModel\";N;s:41:\"\0Gloudemans\\Shoppingcart\\CartItem\0taxRate\";i:21;}}}', NULL, NULL),
(5, 'herman.brandyn@example.com', 'default', 'O:29:\"Illuminate\\Support\\Collection\":1:{s:8:\"\0*\0items\";a:1:{s:32:\"eef12573176125ce53e333e13d747a17\";O:32:\"Gloudemans\\Shoppingcart\\CartItem\":8:{s:5:\"rowId\";s:32:\"eef12573176125ce53e333e13d747a17\";s:2:\"id\";i:12;s:3:\"qty\";i:1;s:4:\"name\";s:18:\"Tiramisu Roll Cake\";s:5:\"price\";d:110;s:7:\"options\";O:39:\"Gloudemans\\Shoppingcart\\CartItemOptions\":1:{s:8:\"\0*\0items\";a:0:{}}s:49:\"\0Gloudemans\\Shoppingcart\\CartItem\0associatedModel\";N;s:41:\"\0Gloudemans\\Shoppingcart\\CartItem\0taxRate\";i:21;}}}', NULL, NULL),
(6, 'orville76@example.org', 'default', 'O:29:\"Illuminate\\Support\\Collection\":1:{s:8:\"\0*\0items\";a:1:{s:32:\"8b486433ba8a9e4089eafa927840a692\";O:32:\"Gloudemans\\Shoppingcart\\CartItem\":8:{s:5:\"rowId\";s:32:\"8b486433ba8a9e4089eafa927840a692\";s:2:\"id\";i:13;s:3:\"qty\";s:1:\"2\";s:4:\"name\";s:6:\"aaaasa\";s:5:\"price\";d:2223;s:7:\"options\";O:39:\"Gloudemans\\Shoppingcart\\CartItemOptions\":1:{s:8:\"\0*\0items\";a:0:{}}s:49:\"\0Gloudemans\\Shoppingcart\\CartItem\0associatedModel\";N;s:41:\"\0Gloudemans\\Shoppingcart\\CartItem\0taxRate\";i:21;}}}', NULL, NULL),
(7, 'ccronin@example.com', 'default', 'O:29:\"Illuminate\\Support\\Collection\":1:{s:8:\"\0*\0items\";a:2:{s:32:\"8b486433ba8a9e4089eafa927840a692\";O:32:\"Gloudemans\\Shoppingcart\\CartItem\":8:{s:5:\"rowId\";s:32:\"8b486433ba8a9e4089eafa927840a692\";s:2:\"id\";i:13;s:3:\"qty\";i:1;s:4:\"name\";s:6:\"aaaasa\";s:5:\"price\";d:2223;s:7:\"options\";O:39:\"Gloudemans\\Shoppingcart\\CartItemOptions\":1:{s:8:\"\0*\0items\";a:0:{}}s:49:\"\0Gloudemans\\Shoppingcart\\CartItem\0associatedModel\";N;s:41:\"\0Gloudemans\\Shoppingcart\\CartItem\0taxRate\";i:21;}s:32:\"eef12573176125ce53e333e13d747a17\";O:32:\"Gloudemans\\Shoppingcart\\CartItem\":8:{s:5:\"rowId\";s:32:\"eef12573176125ce53e333e13d747a17\";s:2:\"id\";i:12;s:3:\"qty\";i:1;s:4:\"name\";s:18:\"Tiramisu Roll Cake\";s:5:\"price\";d:110;s:7:\"options\";O:39:\"Gloudemans\\Shoppingcart\\CartItemOptions\":1:{s:8:\"\0*\0items\";a:0:{}}s:49:\"\0Gloudemans\\Shoppingcart\\CartItem\0associatedModel\";N;s:41:\"\0Gloudemans\\Shoppingcart\\CartItem\0taxRate\";i:21;}}}', NULL, NULL),
(8, 'irish123@gmail.com', 'default', 'O:29:\"Illuminate\\Support\\Collection\":1:{s:8:\"\0*\0items\";a:3:{s:32:\"8b486433ba8a9e4089eafa927840a692\";O:32:\"Gloudemans\\Shoppingcart\\CartItem\":8:{s:5:\"rowId\";s:32:\"8b486433ba8a9e4089eafa927840a692\";s:2:\"id\";i:13;s:3:\"qty\";i:1;s:4:\"name\";s:6:\"aaaasa\";s:5:\"price\";d:2223;s:7:\"options\";O:39:\"Gloudemans\\Shoppingcart\\CartItemOptions\":1:{s:8:\"\0*\0items\";a:0:{}}s:49:\"\0Gloudemans\\Shoppingcart\\CartItem\0associatedModel\";N;s:41:\"\0Gloudemans\\Shoppingcart\\CartItem\0taxRate\";i:21;}s:32:\"eef12573176125ce53e333e13d747a17\";O:32:\"Gloudemans\\Shoppingcart\\CartItem\":8:{s:5:\"rowId\";s:32:\"eef12573176125ce53e333e13d747a17\";s:2:\"id\";i:12;s:3:\"qty\";i:2;s:4:\"name\";s:18:\"Tiramisu Roll Cake\";s:5:\"price\";d:110;s:7:\"options\";O:39:\"Gloudemans\\Shoppingcart\\CartItemOptions\":1:{s:8:\"\0*\0items\";a:0:{}}s:49:\"\0Gloudemans\\Shoppingcart\\CartItem\0associatedModel\";N;s:41:\"\0Gloudemans\\Shoppingcart\\CartItem\0taxRate\";i:21;}s:32:\"620d670d95f0419e35f9182695918c68\";O:32:\"Gloudemans\\Shoppingcart\\CartItem\":8:{s:5:\"rowId\";s:32:\"620d670d95f0419e35f9182695918c68\";s:2:\"id\";i:11;s:3:\"qty\";i:1;s:4:\"name\";s:14:\"Pink Ice Cream\";s:5:\"price\";d:99;s:7:\"options\";O:39:\"Gloudemans\\Shoppingcart\\CartItemOptions\":1:{s:8:\"\0*\0items\";a:0:{}}s:49:\"\0Gloudemans\\Shoppingcart\\CartItem\0associatedModel\";N;s:41:\"\0Gloudemans\\Shoppingcart\\CartItem\0taxRate\";i:21;}}}', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hash` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `hash`, `isAdmin`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'irish', 'irish123@gmail.com', '$2y$10$QlqA9tnbJTaP/wpCc1LPYuBj5UX/gf/2yHXMoGqK0KjmWiyxUh3TG', '', 1, 'tatjFs5QCmIa1b7ICIV3S2K76HbnJNhzXmsAYX0DNGMco5fpvBRWXcr5bgX4', '2018-03-15 04:06:43', '2018-03-15 04:06:43'),
(2, 'eloisa', 'eloisa111@gmail.com', '$2y$10$wEvAUghvUw2lwHzH7vdPQe7xh196.1hVLpt3Fo0LA1rwlK35oUgnG', '', 0, NULL, '2018-03-16 01:04:43', '2018-03-16 01:04:43'),
(3, 'eloi', 'eli222@gmail.com', '$2y$10$xJtnUPOjXXag98nZAvuoae00iIKV/LSSfxJ4kJxFZG6RxJ45dc6Ia', '', 0, 'dDZbvjgFotSrFB5PUtGVyvlAVJEReLLdQVENHGzDrU8uQ9BmDJlLElHghVSI', '2018-03-17 04:09:36', '2018-03-17 04:09:36'),
(4, 'abc', 'abc@gmail.com', '$2y$10$Y1wEnKmGa/P5zcppViPUGOVpdPP9jAJaR9pYiL9/RaH8hL/rhAYAy', 'BFmG79', 0, 'vExageID3aFAB3LlAQamu0DuRxfNyCFXhDMGmyOtmAoESj4zBzMEMGJJYkUy', '2018-06-19 07:05:07', '2018-06-19 07:05:07'),
(6, 'Mr. Art Rowe IV', 'gwendolyn.hilll@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'T0f6R7', 0, 'Y0RAurjDBD', '2018-06-19 07:45:21', '2018-06-19 07:45:21'),
(7, 'Prof. Cullen Greenfelder PhD', 'pdooley@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'qGHieu', 0, 'y34MqOM8Jy', '2018-06-19 07:45:21', '2018-06-19 07:45:21'),
(8, 'Kaela Haley', 'herman.brandyn@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'fITfuj', 0, 'XnjwvErUKZNaCa3LN0Y3N9W4QmuCquu9NLOsPzHtfKZEcxLn8cpQLscK1kOv', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(9, 'Tod Tremblay', 'orville76@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'FLbOVH', 0, '54KVaYw06d', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(10, 'Mazie Hudson', 'hwiegand@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'Xq0dth', 0, '0NQsBd42QMR6x3DfXYycpprpFq0e47lNWKu8VzDSLBO159wU2neHZrOvoaoz', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(11, 'Evelyn Gaylord DDS', 'jordyn.renner@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'PYGAtF', 0, '02gh7wWqizDhTlWhKFDUHVj0HeOrSoftbyoUcjBiN2ttUK7zpy69N5RwnAY1', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(12, 'Ms. Bethany Ratke Sr.', 'ccronin@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'lxAwGQ', 0, 'N75nDx5N0F', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(13, 'Troy Wiegand DDS', 'halvorson.gerardo@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'AUHoaE', 0, 'W7ztW9J17Q', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(14, 'Bessie Cruickshank I', 'bernard69@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'IHu3JO', 0, '0nNPhrW2OI', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(15, 'Eveline Glover', 'dibbert.angelo@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'sWBIZG', 0, 's1YCG25Bt8', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(16, 'Eloy Klein', 'blanca.frami@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'mvLeOt', 0, 'wu4Dj3URbP', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(17, 'Nasir Schmidt DVM', 'konopelski.dejuan@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'e5BMKr', 0, 'iLb9irFc2a', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(18, 'Ms. Alice Ullrich DDS', 'xwilderman@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'j35oln', 0, 'GnOkR04cHx', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(19, 'Sidney Cummerata', 'vilma.cormier@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'iu7E8G', 0, '0MUOzGfd2g', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(20, 'Audreanne Bode', 'larry92@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'kWc4dd', 0, '72M9wvKuxp', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(21, 'Guido Boyer', 'lesch.sonya@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'uSHzGv', 0, 'jhKZzBjrK5', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(22, 'Hudson Gorczany', 'wilhelm.zulauf@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '7fGqqx', 0, '74qHl9fJVt', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(23, 'Krystal Bayer', 'hector.beer@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'n7P9J3', 0, 'eFLAf53EJp', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(24, 'Prof. Bertrand Luettgen', 'dillon76@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '1Z5dGD', 0, 'k1cv5hozMj', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(25, 'Lew Schimmel', 'kschinner@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'mXOirm', 0, 'ByQcXEQfaV', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(26, 'Mr. Lazaro Ullrich Sr.', 'josue.lubowitz@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'Dgubfb', 0, '4i1cY5fggL', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(27, 'Wilford Ullrich Sr.', 'jzulauf@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'hImyOy', 0, 'oI9lwzzSLz', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(28, 'Angelica Hartmann I', 'von.rosendo@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '27LaOq', 0, 'aIlpVRrAaZ', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(29, 'Mrs. Alfreda Green MD', 'xberge@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'IZZekY', 0, 'MDOiR1q4CH', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(30, 'Prof. Dewitt Satterfield IV', 'eyundt@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'PNgyUM', 0, 'boxrLjisqc', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(31, 'Jadyn Weber', 'rodriguez.audie@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'xqdkEa', 0, 'rnnpHCjgH4', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(32, 'Prof. Zachery Macejkovic', 'moen.elvera@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'fRTkb0', 0, 'UDVqrnVZ3W', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(33, 'Ms. Rosina Yost', 'rzieme@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'fLfPFK', 0, 'YbJqZ1kZ7L', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(34, 'Prof. Ottilie Bergnaum Jr.', 'aubrey76@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'mbg7mD', 0, 'CledofVEur', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(35, 'Wanda Simonis', 'champlin.shawna@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '13CAFM', 0, 'K06HqRuDFd', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(36, 'Prof. Molly Satterfield PhD', 'dagmar.weber@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'cyoIaO', 0, 'Ge4oTeppZb', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(37, 'Fanny Gibson', 'judd46@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'ZYxwFs', 0, 'O4vqwnvOKP', '2018-06-19 07:45:22', '2018-06-19 07:45:22'),
(38, 'Nedra Crist', 'beahan.bridie@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'WXZ57o', 0, 'WUI9r1rnum', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(39, 'Edwin West', 'reichert.stanford@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'w7Tqnc', 0, 'CLMAKdkd7Z', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(40, 'Alvena Baumbach', 'fpollich@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'rAdcvg', 0, 'gGhHpopje9', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(41, 'Ms. Maximillia Kautzer IV', 'bradley83@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'Bw7pzE', 0, 'lU1O7d0Ijr', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(42, 'Dr. Luella Gerhold', 'torp.addie@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'auQkds', 0, 'GXocZYpoR0', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(43, 'Cheyanne Halvorson', 'ziemann.casimir@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'QVdlLm', 0, 'pGzgeTfBsU', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(44, 'Irma Bailey', 'larkin.garnet@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'VbsNX1', 0, 'vP9wsdNqYc', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(45, 'Jadyn Kohler', 'ellie70@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'zJCiq3', 0, 'sbKRWcmqqA', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(46, 'Nicola Hermann II', 'alphonso91@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'bRbZkf', 0, '2gHJziXCxm', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(47, 'Bud McClure', 'ahmad41@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'zu7SoW', 0, '9WDBsJtJQ8', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(48, 'Prof. Rosa Friesen PhD', 'newell22@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'rgHyoF', 0, 'UEmdvzlVfX', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(49, 'Eliezer Mosciski', 'stephania.green@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'vhEjpm', 0, 'OiVRpTqTTn', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(50, 'Brannon Bahringer', 'kessler.abraham@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'WRxmgi', 0, 'UzyMmL0TXB', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(51, 'Ms. Marcelina Rodriguez DDS', 'lamont55@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'E44t9V', 0, 'OrIuQnYRHz', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(52, 'Mrs. Thalia Erdman', 'jarod25@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'RCeirL', 0, '2Z1kp4sKgD', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(53, 'Ora Haag', 'melvin.bradtke@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'd5k0yY', 0, 'nv0aonjNTR', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(54, 'Mrs. Halie Okuneva', 'armstrong.vivienne@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'Pr418G', 0, 'bneT3hiP4h', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(55, 'Jordan Schroeder PhD', 'karlie.stark@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'pcjPQU', 0, 'FraxOH17hd', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(56, 'Arvilla Mraz', 'johnpaul.stanton@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'knupyQ', 0, '8FTTKXFeEk', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(57, 'Felicity Howell', 'electa.cruickshank@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '7m4Czw', 0, 'PIeIrCSpv3', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(58, 'Miss Audra Ritchie II', 'kglover@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'xePl9t', 0, 'BJ6almrw3m', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(59, 'Prof. Art Leuschke', 'mcdermott.erich@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'YiYyhR', 0, 'XyMkvtKf19', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(60, 'Houston Friesen', 'twalker@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '1PDd4W', 0, 'ZJEjZxIwla', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(61, 'Mr. Damion Rogahn', 'gwalker@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'SfkP5W', 0, 'CK3RBViVEX', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(62, 'Ezra Kohler', 'fkoepp@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'Qxzamo', 0, 'PBKGKodHMZ', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(63, 'Prof. Keshaun Wolf Sr.', 'amueller@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '50zDzi', 0, '9zrDqYZFKX', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(64, 'Juwan Cremin', 'jacques51@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'G39iab', 0, 'Aey0vf0BzP', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(65, 'Brendan Cassin', 'fdamore@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'o4ALrQ', 0, 'JvmtkXmXvr', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(66, 'Dr. Erwin Kunze', 'ghalvorson@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'AAO5jo', 0, '9PBeu4HrVC', '2018-06-19 07:45:23', '2018-06-19 07:45:23'),
(67, 'Trevion Howe DDS', 'benjamin.cruickshank@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'V9Zyd6', 0, 'xDEAWC2cA2', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(68, 'Cassandre Ernser', 'vandervort.jevon@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'gM7DqI', 0, 'TLngE0K9RR', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(69, 'Thea Rogahn', 'nicolas.aaliyah@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'df8Tz6', 0, '6ikaFNgvQP', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(70, 'Sophia Walsh', 'zpadberg@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'IeZ2GD', 0, 'zb6F0Y8HPr', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(71, 'Ruthe Murray', 'lenny.koch@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'btUeVU', 0, '3PG4E2nOCK', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(72, 'Ms. Hope Emard', 'maybell30@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'O7rk57', 0, 'ChrDM42Qvq', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(73, 'Janelle Crooks', 'oconnell.dexter@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 't8CQQY', 0, '0jsJCMFwFb', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(74, 'Alisha Hodkiewicz I', 'osborne10@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '8goxMK', 0, 'kDjYYWIVV4', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(75, 'Dr. Mohamed Thiel', 'qkulas@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'la2Yd2', 0, 'hqlEOutN61', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(76, 'Linwood Welch', 'yrath@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '8pFu2c', 0, 'oVI1stpdjW', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(77, 'Eric Ruecker', 'weissnat.darrin@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'X7PPzS', 0, 'OIeiLmBOuI', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(78, 'Dianna Harber PhD', 'verdie75@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'GSQVph', 0, 'mdATnnBSKV', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(79, 'Dr. Bradly Fisher', 'lwest@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'i2fkAm', 0, 'MLYvGjVOn0', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(80, 'Katarina Buckridge', 'raquel.bosco@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'WYrp1F', 0, '75cCXDi3sh', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(81, 'Rowland Gorczany MD', 'gunner61@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'ySCIoc', 0, 'xh9dN0KKwR', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(82, 'Mr. Alex Paucek III', 'ward.vesta@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'HAyWA0', 0, 'DfVSOrUU4T', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(83, 'Bernadine Fadel', 'pjerde@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'xYrcqF', 0, 'eJvl0VYZLo', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(84, 'Ms. Bettie Rohan', 'annie.brown@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'agpmkn', 0, 'smJXroff8o', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(85, 'Ms. Clemmie Emard', 'jewell38@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'OsB0nc', 0, 'kSaPCaTylC', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(86, 'Alf Leffler', 'elmore.spencer@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'DYYcDe', 0, '0upCFd8b4m', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(87, 'Kyler Feil', 'hmcdermott@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '55pv5Q', 0, 'iht1HjbPJe', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(88, 'Miss Katrina McLaughlin III', 'ken15@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'kQGWjv', 0, 'S1FwyigmcU', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(89, 'Thaddeus Boyer', 'jast.gideon@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0rze4z', 0, 'YngWkbWX93', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(90, 'Stephan Schmeler', 'stephen.oconner@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'SJSYHu', 0, 'ZXgxKWRQlK', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(91, 'Arne Huel', 'kdamore@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'UxSc5L', 0, 'iicMHzLGop', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(92, 'Eliseo Murphy', 'reinger.josie@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '3KmQOz', 0, 'doJp1Pja8q', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(93, 'Estevan Carter', 'pearlie62@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'iSksHR', 0, 'bXmWMxQ0sg', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(94, 'Nels Kutch V', 'alize.green@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'IkFOCC', 0, 'v3FOanYyXq', '2018-06-19 07:45:24', '2018-06-19 07:45:24'),
(95, 'Angelina Toy PhD', 'btillman@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'wj7hVt', 1, 'v9LH2YlFgmwSGo1i6bWpLaCaNYOWdwKDgGlhdarNx2llelVqjNFMLMSy7O5c', '2018-06-19 07:45:25', '2018-06-19 07:50:06'),
(96, 'Fae Pfannerstill', 'marquardt.orland@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'mWpK3p', 1, 'y5D3NBNF2L', '2018-06-19 07:45:25', '2018-09-05 09:55:20'),
(97, 'Ezekiel Treutel', 'rose33@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '48TWrD', 1, 'nZT1DVT0SA', '2018-06-19 07:45:25', '2018-10-23 02:32:38'),
(98, 'Cedrick Hegmann', 'johnson.keara@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '8ZkrIc', 0, 'rvjO8np5dX', '2018-06-19 07:45:25', '2018-06-19 07:45:25'),
(99, 'Jazmin O\'Reilly', 'houston.bahringer@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'Ej5xYh', 0, 'DGUYEv0vbM', '2018-06-19 07:45:25', '2018-06-19 07:45:25'),
(100, 'Prof. Johnny Lehner', 'vkihn@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'PvWbOr', 0, 'WaYePV88CP', '2018-06-19 07:45:25', '2018-06-19 07:45:25'),
(101, 'Breana Howell', 'carissa00@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'n7vfKS', 0, 'F0qQ60cEKf', '2018-06-19 07:45:25', '2018-06-19 07:45:25'),
(102, 'Cecilia Goldner I', 'erick08@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'l5BlQV', 0, 'In5W02Uu9S', '2018-06-19 07:45:25', '2018-06-19 07:45:25'),
(103, 'Caitlyn Marks MD', 'mueller.beatrice@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'dzYRPi', 0, 'wLDk5nV3MJ', '2018-06-19 07:45:25', '2018-06-19 07:45:25'),
(104, 'Cecelia Stamm', 'marcelina.hodkiewicz@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'OqP5ZU', 0, 'E4Ck9PVP35', '2018-06-19 07:45:25', '2018-06-19 07:45:25'),
(105, 'Dayna Emmerich', 'schuppe.elenora@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'I5Zk5p', 0, 'p7Ta1uKogI', '2018-06-19 07:45:25', '2018-06-19 07:45:25'),
(106, 'jason siervo', 'jason@gmail.com', 'password', '', 1, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shoppingcart`
--
ALTER TABLE `shoppingcart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `shoppingcart`
--
ALTER TABLE `shoppingcart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
