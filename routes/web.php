<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('auth')->group(function(){
    Route::get('/cart/add/{id}', 'User\CartController@add');
    Route::get('/cart', 'User\CartController@display');
    Route::view('/profile','user.profile');
    Route::get('/cart/clear', 'User\CartController@clear');
    Route::get('/cart/delete/{id}', 'User\CartController@delete');
    Route::get('/cart/edit/{id}', 'User\CartController@edit');
    Route::post('/cart/update/{id}', 'User\CartController@update');
    Route::post('/cart/store', 'User\CartController@store');
});

Route::middleware(['auth','admin'])->group(function(){
    Route::get('admin/dashboard', 'DashboardController@index');
    Route::get('admin/user/{id}/admin', 'Admin\UserController@admin');
    Route::get('admin/users/', 'Admin\UserController@index');
    Route::get('admin/user/{id}', 'Admin\UserController@display');
    Route::get('admin/orders/', 'Admin\CartController@orders');
    Route::get('admin/order/{id}', 'Admin\CartController@cart');
    Route::get('admin/items/', 'Admin\ItemController@index');
    Route::get('admin/items/create', 'Admin\ItemController@create');
    Route::post('admin/items/create', 'Admin\ItemController@store');
    Route::get('admin/item/{id}', 'Admin\ItemController@show');
    Route::get('admin/item/{id}/edit', 'Admin\ItemController@edit');
    Route::post('admin/item/{id}/update', 'Admin\ItemController@update');
    Route::post('admin/item/{id}/delete', 'Admin\ItemController@destroy');
});

Route::get('/paypal', function () {
    return view('paywithpaypal');
});

Route::post('paypal', 'payments\PaymentsController@paywithpaypal');

Route::get('status', 'payments\PaymentsController@getPaymentStatus');